package ru.t1.sarychevv.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.TaskDTO;

@Getter
@Setter
@NoArgsConstructor
public class TaskGetByIndexResponse extends AbstractTaskResponse {

    public TaskGetByIndexResponse(@Nullable final TaskDTO task) {
        super(task);
    }

}

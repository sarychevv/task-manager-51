package ru.t1.sarychevv.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.sarychevv.tm.enumerated.Sort;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId,
                    @Nullable Sort sort) throws Exception;

    @NotNull
    M removeOneById(@Nullable String userId,
                    @Nullable String id) throws Exception;

    @Nullable
    M removeOneByIndex(@Nullable String userId,
                       @Nullable Integer id) throws Exception;

    void removeAll(@Nullable String userId) throws Exception;

    @NotNull
    M changeStatusById(@Nullable String userId,
                       @Nullable String id,
                       @Nullable Status status) throws Exception;

    @NotNull
    M changeStatusByIndex(@Nullable String userId,
                          @Nullable Integer index,
                          @Nullable Status status) throws Exception;

    @NotNull
    M updateById(@Nullable String userId,
                 @Nullable String id,
                 @Nullable String name,
                 @Nullable String description) throws Exception;

    @NotNull
    M updateByIndex(@Nullable String userId,
                    @Nullable Integer index,
                    @Nullable String name,
                    @Nullable String description) throws Exception;

}


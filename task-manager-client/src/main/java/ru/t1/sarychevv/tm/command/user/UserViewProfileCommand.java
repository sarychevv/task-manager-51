package ru.t1.sarychevv.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.model.UserDTO;
import ru.t1.sarychevv.tm.dto.request.user.UserProfileRequest;

public class UserViewProfileCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String getDescription() {
        return "View user profile.";
    }

    @NotNull
    @Override
    public String getName() {
        return "view-user-profile";
    }

    @Override
    public void execute() {
        @NotNull final UserProfileRequest request = new UserProfileRequest(getToken());
        @Nullable final UserDTO user = getAuthEndpoint().profile(request).getUser();
        System.out.println("[USER VIEW PROFILE]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("LAST NAME: " + user.getLastName());
        System.out.println("E-MAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
